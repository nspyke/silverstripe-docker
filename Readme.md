# SilverStripe Docker images

Docker images suitable for SilverStripe project CI/CD pipelines

- PHP 5.6, 7.1, 7.2
- Contains all required extensions for SilverStripe
- Composer